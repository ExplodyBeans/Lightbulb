love.graphics.setDefaultFilter("nearest", "nearest")
love.graphics.setFont(love.graphics.newImageFont("assets/font.png", "1234567890"))

local power = 3
local minPower = 1
local midPower = 5
local maxPower = 10
local breakPower = 24
local cutoffPower = 2

local powerDrain = 1.25
local powerDrainMin = 3
local powerPerClick = 2

local score = 0
local scoreMultiplier = 1
local STARTED = false

local timeSinceClick = 1
local minTimeBetweenClick = 0.001
local maxTimeBetweenClick = 1

local minClickPitch = 0.5
local maxClickPitch = 1.2

local glowColor = {r=255, g=226, b=117}

local w, h = love.graphics.getDimensions()
w = math.ceil(w / 8)
h = math.ceil(h / 8)
local halfW, halfH = math.ceil(w/2), math.ceil(h/2)

local bwaSound = love.audio.newSource("assets/bwaaaaah.wav", "static")
bwaSound:setVolume(2)
local ambientSFX = love.audio.newSource("assets/ambient_generator.wav", "static")
ambientSFX:setVolume(0.1)
ambientSFX:setLooping(true)
ambientSFX:play()
local audioScale = 1

local button = {
  left=23,
  right=40,
  top=42,
  bottom=53,
  state="idle",
  sprites={
    idle=love.graphics.newImage("assets/button_idle.png"),
    hovered=love.graphics.newImage("assets/button_hovered.png"),
    pressed=love.graphics.newImage("assets/button_pressed.png")
  }
}

local bulb = {
  broken=false,
  dead=false,
  sprites={
    off=love.graphics.newImage("assets/bulb_off.png"),
    on=love.graphics.newImage("assets/bulb_on.png"),
    broken=love.graphics.newImage("assets/bulb_broken.png")
  }
}

local consoleImage = love.graphics.newImage("assets/console.png")

local buttonPressedPool = {}
for i=1, 10 do
  local source = love.audio.newSource("assets/Button_Pressed_01.wav", "static")
  table.insert(buttonPressedPool, source)
end

local buttonReleasedPool = {}
for i=1, 10 do
  local source = love.audio.newSource("assets/Button_Released_01.wav", "static")
  table.insert(buttonReleasedPool, source)
end

function getClickPitch()
  local ratio = (timeSinceClick - minTimeBetweenClick) / maxTimeBetweenClick
  return minClickPitch + ((-ratio+1) * maxClickPitch)
end

local function playButtonPressed()
  local pitch = getClickPitch() + math.random() * 0.3

  local oldestSound = nil
  local mostSamples = 0
  local found = false
  for i=1, 10 do
    local source = buttonPressedPool[i]
    if source:isStopped() then
      source:setPitch(pitch)
      source:play()
      found = true
      break
    else
      local samples = source:tell("samples")
      if samples > mostSamples then
        mostSamples = samples
        oldestSound = source
      end
    end
  end
  if not found then
    source:setPitch(pitch)
    oldestSound:rewind()
  end
end

local function playButtonReleased()
  local pitch = getClickPitch() + math.random() * 0.3

  local oldestSound = nil
  local mostSamples = 0
  local found = false
  for i=1, 10 do
    local source = buttonReleasedPool[i]
    if source:isStopped() then
      source:setPitch(pitch)
      source:play()
      found = true
      break
    else
      local samples = source:tell("samples")
      if samples > mostSamples then
        mostSamples = samples
        oldestSound = source
      end
    end
  end
  if not found then
    source:setPitch(pitch)
    oldestSound:rewind()
  end
end

function lerp(a,b,t) return (1-t)*a + t*b end


function love.load()

end

function love.update(dt)
  if STARTED then
    power = power - math.max((power / powerDrain) + ((score / 20) ^ 0.9), powerDrainMin) * dt
    power = math.max(power, minPower)
    if power >= breakPower then
      bulb.broken = true
      power = power / 2
      bwaSound:play()
    end
    if power <= cutoffPower and not bulb.dead and not bulb.broken then
      bulb.dead = true
      bwaSound:play()
    end
    audioScale = lerp(audioScale, power / maxPower, dt * 10)
    if audioScale > 0 then
      ambientSFX:setVolume((audioScale * 2) ^ 5)
      ambientSFX:setPitch((audioScale * 3))
    end
    if not bulb.broken and not bulb.dead then
      score = score + dt * (power / maxPower) * 2
    end
    timeSinceClick = timeSinceClick + dt
    if timeSinceClick > maxTimeBetweenClick then
      timeSinceClick = maxTimeBetweenClick
    end
  end
end

function love.draw()
  local lightMultiplier = (minPower / maxPower)
  local shade = 255 * lightMultiplier
  if not bulb.broken then
    lightMultiplier = (power / maxPower)
    shade = 255 * lightMultiplier
    love.graphics.setBlendMode("add")

    love.graphics.setColor(glowColor.r * lightMultiplier, glowColor.g * lightMultiplier, glowColor.b * lightMultiplier, 30 * lightMultiplier)
    love.graphics.rectangle("fill", 0, 0, w, h)
    local glowY = 19
    love.graphics.circle("fill", halfW, glowY, halfW * (power / midPower / 2))
    love.graphics.circle("fill", halfW, glowY, halfW * (power / midPower / 4))
    love.graphics.circle("fill", halfW, glowY, halfW * (power / midPower / 8))

    love.graphics.setBlendMode("alpha")

    love.graphics.setColor(255, 255, 255, 255 * (-lightMultiplier+1))
    love.graphics.draw(bulb.sprites.off)

    love.graphics.setColor(shade, shade, shade, 255 * lightMultiplier)
    love.graphics.draw(bulb.sprites.on)
  else
    love.graphics.setColor(shade, shade, shade, 255)
    love.graphics.draw(bulb.sprites.broken)
  end

  love.graphics.setColor(shade, shade, shade, 255)
  love.graphics.draw(consoleImage)
  love.graphics.draw(button.sprites[button.state])
  love.graphics.print(tostring(math.ceil(score)), 0, 0)
end

function love.mousepressed(x, y, btn)
  x = math.floor(x / 8)
  y = math.floor(y / 8)
  if x >= button.left and x <= button.right and
    y >= button.top and y <= button.bottom then
    if not STARTED then
      STARTED = true
    end
    if not bulb.broken and not bulb.dead and STARTED then
      power = power + powerPerClick
    end
    button.state = "pressed"
    playButtonPressed()
    timeSinceClick = 0
  end
end

function love.mousereleased(x, y, btn)
  if button.state == "pressed" then
    playButtonReleased()
  end
  button.state = "idle"
  love.mousemoved(x, y, btn)
end

function love.mousemoved(x, y, btn)
  x = math.floor(x / 8)
  y = math.floor(y / 8)
  if button.state ~= "pressed" then
    if x >= button.left and x <= button.right and
      y >= button.top and y <= button.bottom then
      button.state = "hovered"
    else
      button.state = "idle"
    end
  end
end

function love.run()
  if love.math then
    love.math.setRandomSeed(os.time())
    math.randomseed(os.time())
  end
 
  if love.load then love.load(arg) end

  local canvas = nil
  if love.graphics and love.graphics.isActive() then
    local width, height = love.graphics.getDimensions()
    canvas = love.graphics.newCanvas(math.ceil(width / 8), math.ceil(height / 8))
  end
 
  -- We don't want the first frame's dt to include time taken by love.load.
  if love.timer then love.timer.step() end
 
  local dt = 0
 
  -- Main loop time.
  while true do
    -- Process events.
    if love.event then
      love.event.pump()
      for name, a,b,c,d,e,f in love.event.poll() do
        if name == "quit" then
          if not love.quit or not love.quit() then
            return a
          end
        end
        love.handlers[name](a,b,c,d,e,f)
      end
    end
 
    -- Update dt, as we'll be passing it to update
    if love.timer then
      love.timer.step()
      dt = love.timer.getDelta()
    end
 
    -- Call update and draw
    if love.update then love.update(dt) end -- will pass 0 if love.timer is disabled
 
    if love.graphics and love.graphics.isActive() then
      love.graphics.clear(love.graphics.getBackgroundColor())
      love.graphics.setCanvas(canvas)
        love.graphics.clear(13, 13, 15, 255)
        love.graphics.origin()
        if love.draw then love.draw() end
      love.graphics.setCanvas()
      love.graphics.setColor(255, 255, 255, 255)
      love.graphics.draw(canvas, 0, 0, 0, 8, 8)
      love.graphics.present()
    end
 
    if love.timer then love.timer.sleep(0.001) end
  end
end